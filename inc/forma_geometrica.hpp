#ifndef FORMA_GEOMETRICA_HPP
#define FORMA_GEOMETRICA_HPP

#include <string>

using namespace std;

class FormaGeometrica {

private:
	float base;
	float altura;
	string tipo;
public:
	FormaGeometrica();
	FormaGeometrica(float base, float altura);
	~FormaGeometrica();
	
	void setBase(float base);
	float getBase();
	void setAltura(float altura);
	float getAltura();
	void setTipo(string tipo);
	string getTipo();

	float calculaArea();
	float calculaPerimetro();
};



#endif
